$(function () {
    R.noti.init();
    R.modal.init();
    R.init.base();
    Own.init.base();

    var $jsLauncher = $('#js-launcher');
    if ($jsLauncher.length && $jsLauncher.val() !== '') {
        if (typeof R.init[$jsLauncher.val()] === 'function') {
            R.init[$jsLauncher.val()]();
        } else if (R.isDev()) {
            console.log('missing R js-launcher: ' + $jsLauncher.val());
        }
        if (typeof Own.init[$jsLauncher.val()] === 'function') {
            Own.init[$jsLauncher.val()]();
        } else if (R.isDev()) {
            console.log('missing Own js-launcher: ' + $jsLauncher.val());
        }
    }
});

(function (R) {
    R.const = {
        pending: 0,
        published: 1,
        updating: 2,
        publishing: 3,
        deleted: 4,
        old: 5,
        search: 83 // s
    };

    R.layout = $('#rb-layout');

    R.init = {
        base: function () {
            $('#rb-burger').click(function (e) {
                e.preventDefault();
                $('#rb-nav').toggleClass('rb-nav-mobile');
            });

            // Up arrow
            $('#up').click(function (e) {
                e.preventDefault();
                $(window.opera ? 'html' : 'html, body').animate({scrollTop: 0}, 'slow');
            });

            // search
            $('#r-search').click(function (e) {
                e.preventDefault();
                R.tools.search();
            });

            $(document).bind('keyup', function (e) {
                if ($('input').is(':focus') || $('select').is(':focus') || $('textarea').is(':focus')) {
                    return;
                }

                if (e.keyCode === R.const.search) {
                    R.tools.search();
                }
            });

            $('#modal-rebond').on('keyup', '#search-content', function () {
                var search = $(this).val();
                if (search === '') {
                    return;
                }

                R.delay(function () {
                    R.service.search(search);
                }, 500);
            });

            R.layout.on('click', 'a[class|=status]', function (e) {
                e.preventDefault();
                var $item = $(this);
                if ($item.data('status') === undefined) {
                    return;
                }

                // confirm deletion
                if ($item.data('status') === 3 && !$item.data('skip')) {
                    if (!confirm(R.lang('item_delete', [$item.data('entity')]))) {
                        return;
                    }
                }
                R.service.updateStatus($item);
            });

            R.activator();
            R.hashSelect();

            // Admin column
            $('#rb-collapse').click(function (e) {
                e.preventDefault();
                $(this).hide();
                $('#rb-nav-side').addClass('rb-nav-side-reduced');
                $('#rb-expand').show();
            });

            var $expand = $('#rb-expand');

            $expand.click(function (e) {
                e.preventDefault();
                $(this).hide();
                $('#rb-nav-side').removeClass('rb-nav-side-reduced');
                $('#rb-collapse').show();
            });

            // back button
            $('.go-back').click(function (e) {
                e.preventDefault();
                window.history.back();
            });

            // Form changed
            R.formChanged = false;
            $('input, textarea, select', 'form').on('change', function () {
                R.formChanged = true;
            });
            $('button').on('click', function () {
                R.formChanged = false;
            });
            $(window).on('beforeunload', function () {
                return R.formChanged ? true : undefined;
            });
        },
        cmsComponent: function () {
            window.onhashchange = R.service.listComponent;
            R.service.listComponent();

            // filters
            R.layout.on('change', '#count', function () {
                var count = $(this).val();
                R.service.updateUserSettings(count, 'paging', 'hash');
            });
        },
        cmsComponentEdit: function () {
            // hide disable type when module without content is selected
            var $moduleId = $('#moduleId');
            R.tools.checkPlugin($moduleId);
            $moduleId.change(function () {
                R.tools.checkPlugin($(this));
            });
        },
        configLang: function () {
            CodeMirror.fromTextArea(document.getElementById('fileContent'), {
                lineNumbers: true,
                mode: {name: 'yaml', alignCDATA: true}
            });
        },
        configCache: function () {
            $('.delete-cache').click(function (e) {
                e.preventDefault();
                R.service.deleteCache($(this));
            });
            $('#delete-all-cache').click(function (e) {
                e.preventDefault();
                R.service.deleteAllCache();
            });
        },
        configMediaLinkEdit: function () {
            var $package = $('#package');
            R.tools.mediaLinkEdit($package);
            $package.change(function () {
                R.tools.mediaLinkEdit($(this));
            });
        },
        contentEditor: function () {
            R.editor.mediaModalSelector();
            R.editor.richText();
            R.editor.datePicker();

            $('#delete').click(function () {
                return confirm(R.lang('item_delete', [R.url.queryMap('module')]));
            });
        },
        contentList: function () {
            // selectors
            $('#module').change(function () {
                R.tools.setContentFilters(true);
            });

            window.onhashchange = R.service.listContent;
            R.service.listContent();
            R.tools.setContentFilters(false);

            // filters
            R.layout.on('change', '#order', function () {
                window.location = $(this).val();
            });

            R.layout.on('change', '#count', function () {
                var count = $(this).val();
                R.service.updateUserSettings(count, 'contentPaging', 'hash');
            });

            // Edit status of App model
            R.layout.on('click', 'a.publish', function (e) {
                e.preventDefault();
                var $$ = $(this);
                var entity = $$.data('entity');
                var appId = $$.data('app');
                R.service.updateVersion(entity, appId, R.const.published);
            });
            R.layout.on('click', 'a.delete', function (e) {
                e.preventDefault();
                var $$ = $(this);
                var entity = $$.data('entity');
                var appId = $$.data('app');
                if (confirm(R.lang('item_delete', [entity]))) {
                    R.service.updateVersion(entity, appId, R.const.deleted);
                }
            });
            R.layout.on('click', 'a.undelete', function (e) {
                e.preventDefault();
                var $$ = $(this);
                var entity = $$.data('entity');
                var appId = $$.data('app');
                R.service.updateVersion(entity, appId, R.const.publishing);
            });
        },
        designerCss: function () {
            CodeMirror.fromTextArea(document.getElementById('fileContent'), {
                lineNumbers: true,
                mode: 'text/x-scss'
            });
        },
        designerTpl: function () {
            CodeMirror.fromTextArea(document.getElementById('fileContent'), {
                lineNumbers: true,
                mode: 'application/x-httpd-php'
            });
        },
        dev: function () {
            $('.delete-backup').click(function (e) {
                e.preventDefault();
                if (!confirm(R.lang('item_delete', [lang.backup]))) {
                    return;
                }
                R.service.deleteBackup($(this));
            });

            $('.restore').click(function () {
                return confirm(lang.backup_restore);
            });

            $('.bin-delete').click(function (e) {
                e.preventDefault();
                R.service.deleteBin($(this));
            });

            $('#generate-filter').click(function (e) {
                e.preventDefault();
                $('.status-success, .status-warning', '#rb-generator-response').toggle();
            });
        },
        devModel: function () {
            CodeMirror.fromTextArea(document.getElementById('fileContent'), {
                lineNumbers: true,
                mode: {name: 'yaml', alignCDATA: true}
            });
        },
        install: function () {
            R.editor.password();
        },
        mediaCrop: function () {
            $('#crop-img').cropper({
                checkCrossOrigin: false,
                viewMode: 2,
                dragMode: 'none',
                movable: false,
                rotatable: false,
                zoomable: false,
                minCropBoxWidth: 20,
                minCropBoxHeight: 20,
                crop: function (e) {
                    $('#x').val(parseInt(e.x));
                    $('#y').val(parseInt(e.y));
                    $('#w').val(parseInt(e.width));
                    $('#h').val(parseInt(e.height));
                }
            });

            $('#btnSaveCrop').click(function () {
                return checkCoords();
            });

            function checkCoords() {
                if ($('#w').val() > 0 && $('#h').val() > 0) {
                    return true;
                }
                alert(lang.image_crop_select);
                return false;
            }
        },
        mediaEdit: function () {
            $('#replace').hide();

            $('#button-replace').click(function (e) {
                e.preventDefault();
                $('#editor').slideUp();
                $('#replace').slideDown();
            });
            $('#cancel-replace').click(function (e) {
                e.preventDefault();
                $('#editor').slideDown();
                $('#replace').slideUp();
            });

            var dropzone = new Dropzone('#media-uploader', {
                url: '/Service/upload',
                maxFilesize: 2,
                thumbnailWidth: 64,
                thumbnailHeight: 64,
                params: {'replaceId': $('#replace-id').val()},
                //acceptedFiles: 'image/!*,test/!*,application/pdf,application/msword,application/vnd.ms*,application/vnd.oasis*',
                autoProcessQueue: false,
                addRemoveLinks: true,
                maxFiles: 1,
                dictDefaultMessage: lang.drop_files_to_upload,
                dictFileTooBig: lang.file_too_big,
                dictInvalidFileType: lang.file_type_invalid,
                dictRemoveFile: lang.remove,
                init: function () {
                    var dz = this;
                    $('#media-upload').click(function (e) {
                        e.preventDefault();
                        dz.processQueue();
                    });
                },
                success: function (file, data) {
                    data = JSON.parse(data);
                    if (data.result === 1) {
                        R.noti.render(data.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    } else {
                        R.noti.error(data.message);
                    }
                }
            });
        },
        mediaView: function () {
            $('#folder-tree').folder({
                expand: '#f_1'
            });

            // edit folder
            $('#folder-edit').click(function () {
                if ($(this).attr('href') === '#') {
                    R.noti.render(lang.folder_select_to_edit);
                    return false;
                }
            });

            // delete folder
            $('#folder-delete').click(function (e) {
                e.preventDefault();
                var id = $('#folder-del').val();
                if (id === 0) {
                    R.noti.error(lang.folder_select_to_delete);
                } else if (id === 1) {
                    R.noti.error(lang.folder_cannot_delete_root);
                } else {
                    if (confirm(R.lang('item_delete', [lang.folder]))) {
                        R.service.deleteFolder(id);
                    }
                }
            });

            // delete media
            R.layout.on('click', 'a.media-delete', function (e) {
                e.preventDefault();
                if (confirm(R.lang('item_delete', [lang.media]))) {
                    R.service.deleteMedia($(this).data('id'));
                }
            });

            // open media in modal
            R.layout.on('click', 'img.thumb', function () {
                var id = $(this).data('id');
                var alt = $(this).attr('alt');
                var img = R.service.getMedia(id);
                R.modal.create('<img src="' + img + '" alt="' + alt + '" />', lang.image);
            });

            // filters
            R.layout.on('change', '#view', function () {
                var view = $(this).val();
                R.service.updateUserSettings(view, 'mediaView', 'hash');
            });

            R.layout.on('change', '#count', function () {
                var count = $(this).val();
                R.service.updateUserSettings(count, 'mediaPaging', 'hash');
            });

            R.layout.on('change', '#order', function () {
                window.location = $(this).val();
            });

            R.layout.on('change', '#type', function () {
                window.location = $(this).val();
            });

            R.layout.on('change', '#selectable', function () {
                window.location = $(this).val();
            });

            window.onhashchange = R.service.listMedia;
            R.service.listMedia();
        },
        mediaUpload: function () {
            var dropzone = new Dropzone('#media-uploader', {
                url: '/Service/upload',
                maxFilesize: 2,
                thumbnailWidth: 64,
                thumbnailHeight: 64,
                params: {'folderId': $('#folder-id').val()},
                //acceptedFiles: 'image/!*,test/!*,application/pdf,application/msword,application/vnd.ms*,application/vnd.oasis*',
                autoProcessQueue: false,
                addRemoveLinks: true,
                dictDefaultMessage: lang.drop_files_to_upload,
                dictFileTooBig: lang.file_too_big,
                dictInvalidFileType: lang.file_type_invalid,
                dictRemoveFile: lang.remove,
                init: function () {
                    var dz = this;
                    $('#media-upload').click(function () {
                        dz.processQueue();
                    });
                },
                success: function (file, data) {
                    data = JSON.parse(data);
                    if (data.result === 1) {
                        R.noti.render(data.message);
                    } else {
                        R.noti.error(data.message);
                    }
                }
            });
        },
        page: function () {
            $('#page-tree').tree({
                expand: '#p_1'
            });

            // edit page
            $('#edit-page').click(function () {
                if ($(this).attr('href') === '/') {
                    R.noti.render(lang.page_select_to_edit);
                    return false;
                }
            });

            // pre fill firendlyurl from title
            $('#title').blur(function () {
                var $friendlyUrl = $('#friendlyUrl');
                if ($friendlyUrl.val() === '') {
                    var slug = R.tools.toFriendlyUrl($(this).val());
                    $friendlyUrl.val(slug);
                }
            });

            // delete page
            $('#delete-page').click(function (e) {
                e.preventDefault();
                var id = parseInt($('#del-page').val());
                if (id === 0) {
                    R.noti.error(lang.page_select_to_delete);
                } else if (id <= 9) {
                    R.noti.error(lang.page_cannot_delete);
                } else {
                    var title = $('#p_' + id).find('span.title').clone().children().remove().end().html();
                    if (confirm(lang.delete + ' ' + title + '. ' + lang.page_all_children)) {
                        R.service.deletePage(id);
                    }
                }
            });

            // edit gadget
            $('#edit-gadget').click(function () {
                if ($(this).attr('href') === '/') {
                    R.noti.error(lang.page_select_to_edit_gadget);
                    return false;
                }
                return true;
            });
        },
        pageGadget: function () {
            R.init.page();

            R.layout.on('change', 'select.column', function () {
                var $select = $(this);
                var id = $select.attr('id').split('_')[1];
                R.service.updateGadget(id, 'column', $select.val());
            });

            R.layout.on('blur', 'input.display-order', function () {
                var $a = $(this);
                var id = $a.data('id');
                R.service.updateGadget(id, 'displayOrder', $a.val());
            });

            R.layout.on('click', 'a.gadget-filter', function (e) {
                e.preventDefault();
                R.service.selectGadgetFilter($(this).data('id'));
            });

            var $modal = $('#modal-rebond');

            $modal.on('keyup', '#search-filter', function () {
                var search = $(this).val();
                R.delay(function () {
                    R.service.selectGadgetFilter($('#content-id').val(), search);
                }, 1000);
            });

            $modal.on('click', 'a.option', function (e) {
                e.preventDefault();
                var $a = $(this);
                var id = $('#content-id').val();
                var filter = $a.data('filter');
                var version = $a.data('version');
                R.service.updateGadget(id, 'filter', filter);
                $('#filter-info-' + id).html($a.html());

                // update href of edit link
                if (version) {
                    var $edit = $('#filter-info-edit-' + id);
                    var module = $edit.data('module');
                    $edit.attr('href', '/content/edit/?module=' + module + '&id=' + version);
                    $edit.html(lang.edit);
                }
                $modal.fadeOut();
                $('#rb-overlay').hide();
            });
        },
        profile: function () {
            R.editor.fileSelector();
            R.editor.password();
        },
        profileHelp: function () {
            $('.help a').click(function (e) {
                e.preventDefault();
                $(this).next('div').slideToggle();
            });
        },
        toolsLogs: function () {
            $('#log-files').change(function () {
                window.location = $(this).val();
            });

            $('.log-stack').click(function (e) {
                e.preventDefault();
                var modal = $(this).data('modal');
                var title = $(this).data('title');
                R.modal.render(modal, title);
            });
        },
        quickview: function () {
            window.onhashchange = R.service.listQuickView;
            R.service.listQuickView();

            // filters
            R.layout.on('change', '#count', function () {
                var count = $(this).val();
                R.service.updateUserSettings(count, 'paging', 'hash');
            });
        },
        quickedit: function () {
            R.editor.mediaModalSelector();
            R.editor.fileSelector();
            R.editor.password();
            R.editor.richText();
            R.editor.datePicker();
        },
        userEdit: function () {
            R.editor.fileSelector();
            R.editor.password();

            R.service.togglePassword();
            $('#withPassword').change(function () {
                R.service.togglePassword();
            });
        },
        userPassword: function () {
            R.editor.password();
        },
        userView: function () {
            window.onhashchange = R.service.listUser;
            R.service.listUser();

            // filters
            R.layout.on('change', '#order', function () {
                window.location = $(this).val();
            });

            // filters
            R.layout.on('change', '#count', function () {
                var count = $(this).val();
                R.service.updateUserSettings(count, 'paging', 'hash');
            });
        }
    };

    // search function
    R.delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    R.service = {
        deleteAllCache: function () {
            R.noti.loading();
            $('.delete-cache').parents('tr').addClass('updating');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/deleteCache'
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        $('.delete-cache').parents('tr').remove();
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [deleteAllCache]');
                });
        },
        deleteBackup: function ($file) {
            R.noti.loading();
            $file.parents('tr').addClass('updating');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/deleteBackup',
                data: 'file=' + $file.data('file')
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        $file.parents('tr').remove();
                        var $itemCount = $('#item-count');
                        $itemCount.html(parseInt($itemCount.html()) - 1);
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [deleteBackup]');
                });
        },
        deleteBin: function (element) {
            R.noti.loading();
            // main template
            if (element.data('folder')) {
                var params = 'folder=' + element.data('folder') + '&file=' + element.data('file');
                // module content & template
            } else if (element.data('module')) {
                if (element.data('id')) {
                    params = 'module=' + element.data('module') + '&contentId=' + element.data('id');
                } else {
                    params = 'module=' + element.data('module') + '&file=' + element.data('file');
                }
                // css
            } else if (element.data('skin')) {
                params = 'skin=' + element.data('skin') + '&file=' + element.data('file');
            }
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/deleteBin',
                data: params
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        element.parents('tr').remove();
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [deleteBin]');
                });
        },
        deleteCache: function ($cache) {
            R.noti.loading();
            $cache.parents('tr').addClass('updating');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/deleteCache',
                data: 'file=' + $cache.data('file')
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        $cache.parents('tr').remove();
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [deleteCache]');
                });
        },
        deleteFolder: function (id) {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/deleteFolder',
                data: 'id=' + id
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        $('#folder-tree').find('.selected').parent('li').remove();
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [deleteFolder]');
                });
        },
        deleteMedia: function (id) {
            R.noti.loading();
            $('#media-' + id).addClass('updating');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/deleteMedia',
                data: 'id=' + id
            })
                .done(function (json) {
                    if (json.result !== 2) {
                        $('#media-' + id).remove();
                        var $itemCount = $('#item-count');
                        $itemCount.html(parseInt($itemCount.html()) - 1);
                        R.noti.render(json.message);
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [deleteMedia]');
                });
        },
        deletePage: function (_id) {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/deletePage',
                data: 'id=' + _id
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        $('.selected', '#page-tree').parent('li').remove();
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [deletePage]');
                });
        },
        getMedia: function (id) {
            var ret = '';
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/getMedia',
                async: false,
                data: 'id=' + id
            })
                .done(function (json) {
                    if (json.result === 1) {
                        ret = json.html;
                    } else if (json.result === 0) {
                        ret = id;
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [getMedia]');
                });
            return ret;
        },
        listComponent: function () {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/listComponent',
                data: 'hash=' + R.url.parseUrl().hash
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        R.layout.html(json.html);
                    } else if (json.result === 0) {
                        R.noti.clear();
                        R.layout.html(json.html);
                    } else {
                        R.noti.error(json.message);
                    }
                    R.activator('component-status');
                    R.activator('paging');
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [listComponent]');
                });
        },
        listContent: function () {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/listContent',
                data: 'hash=' + R.url.parseUrl().hash
            })
                .done(function (json) {
                    if (json.result === 1) {
                        var $statusBar = $('#status-bar');
                        R.noti.render(json.message);
                        $('#r-content').html(json.html);
                        $('#module').val(json.module);
                        $statusBar.removeClass('hide');
                        $('#content-version').data('active', json.tabon);
                    } else if (json.result === 0) {
                        R.noti.clear();
                        $('#r-content').html(json.html);
                    } else {
                        R.noti.error(json.message);
                    }
                    R.activator('content-version');
                    R.activator('paging');
                    R.hashSelect('order');
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [listContent]');
                });
        },
        listMedia: function () {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/listMedia',
                data: 'hash=' + R.url.parseUrl().hash
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        R.layout.html(json.html);
                    } else if (json.result === 0) {
                        R.noti.clear();
                        R.layout.html(json.html);
                    } else {
                        R.noti.error(json.message);
                    }
                    R.activator('paging');
                    R.hashSelect('type');
                    R.hashSelect('order');
                    R.hashSelect('selectable');
                    R.activator('view');
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [listMedia]');
                });
        },
        listQuickView: function () {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/listQuickView',
                data: 'hash=' + R.url.parseUrl().hash
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        $('#nav-quickview').find('a').removeClass('selected');
                        $('#quickview-' + json.module).addClass('selected');
                        R.layout.html(json.html);
                    } else if (json.result === 0) {
                        R.noti.clear();
                        R.layout.html(json.html);
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [listQuickView]');
                });
        },
        listUser: function () {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/listUser',
                data: 'hash=' + R.url.parseUrl().hash
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        R.layout.html(json.html);
                    } else if (json.result === 0) {
                        R.noti.clear();
                        R.layout.html(json.html);
                    } else {
                        R.noti.error(json.message);
                    }
                    R.activator('user-status');
                    R.hashSelect('order');
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [listUser]');
                });
        },
        search: function (search) {
            $('#search-result').html('<i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i>');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/search',
                data: 'search=' + search
            })
                .done(function (json) {
                    $('#search-result').html(json.message);
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [search]');
                });
        },
        selectMedia: function (field, hash, search) {
            $('#media-grid').html('<i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i>');
            var postData = 'field=' + field + '&hash=' + hash;
            if (search !== undefined) {
                postData += '&search=' + search;
            }
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/selectMedia',
                data: postData
            })
                .done(function (json) {
                    if (json.result !== 2) {
                        R.noti.render(json.message);
                        R.modal.create(json.html, lang.medium_select);
                        var $search = $('#search-media');
                        $search.focus();
                        var s = $search.val();
                        $search.val('').val(s);
                    } else {
                        R.noti.error(json.message);
                    }
                    var hash = $('#hash').val();
                    R.activator('paging');
                    R.activator('view');
                    R.hashSelect('folderId', hash);
                    R.hashSelect('type', hash);
                    R.hashSelect('order', hash);

                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [selectMedia]');
                });
        },
        selectGadgetFilter: function (id, search) {
            $('#search-result').html('<i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i>');
            var postData = 'id=' + id;
            if (search !== undefined) {
                postData += '&search=' + search;
            }
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/selectGadgetFilter',
                data: postData
            })
                .done(function (json) {
                    if (json.result !== 2) {
                        R.modal.create(json.html, lang.filter_select);
                        var $search = $('#search-filter');
                        $search.focus();
                        var s = $search.val();
                        $search.val('').val(s);
                    } else {
                        R.noti.error(json.html);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [selectGadgetFilter]');
                });
        },
        tinymce: function () {
            var mediaList = {};
            $.ajax({
                url: '/js/tinymce/mediaList.js',
                type: 'get',
                dataType: 'html',
                async: false,
                success: function (data) {
                    return mediaList = data;
                }
            });
            return mediaList;
        },
        togglePassword: function () {
            if ($('#withPassword').is(':checked')) {
                $('#password-form').show();
            } else {
                $('.password', '#password-form').val('');
                $('#password-form').hide();
            }
        },
        updateGadget: function (id, property, value) {
            R.noti.loading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/updateGadget',
                data: 'id=' + id + '&property=' + property + '&value=' + value
            })
                .done(function (json) {
                    if (json.result !== 2) {
                        R.noti.render(json.message);
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [updateGadget]');
                });
        },
        updateStatus: function ($item) {
            var params = 'package=' + $item.data('package') + '&entity=' + $item.data('entity') + '&id=' + $item.data('id') + '&status=' + $item.data('status');
            if ($item.data('id2')) {
                params += '&id2=' + $item.data('id2');
            }
            R.noti.loading();
            $item.closest('tr').addClass('updating');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/updateStatus',
                data: params
            })
                .done(function (json) {
                    if (json.result === 1) {
                        R.noti.render(json.message);
                        if ($item.hasClass('status-0')) {
                            $item.closest('tr').removeClass('updating');
                            $item.removeClass('status-0')
                                .addClass('status-1')
                                .html(lang.active)
                                .data('status', 1);
                        } else if ($item.hasClass('status-1')) {
                            $item.closest('tr').removeClass('updating');
                            $item.removeClass('status-1')
                                .addClass('status-0')
                                .html(lang.inactive)
                                .data('status', 0);
                        } else {
                            var $itemCount = $('#item-count');
                            if ($itemCount.length) {
                                $itemCount.html(parseInt($itemCount.html()) - 1);
                            }
                            $item.closest('tr').remove();
                        }
                    } else if (json.result === 0) {
                        R.noti.render(json.message);
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [updateStatus]');
                });
        },
        updateUserSettings: function (value, type, nextAction) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/updateUserSettings',
                data: 'value=' + value + '&type=' + type
            })
                .done(function (json) {
                    if (json.result === 1) {
                        if (nextAction === 'hash') {
                            window.dispatchEvent(new HashChangeEvent('hashchange'));
                        } else if (nextAction === 'media-modal') {
                            var field = $('#media-selected').val();
                            var hash = $('#hash').val();
                            var search = $('#search-media').val();
                            R.service.selectMedia(field, hash, search);
                        }
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [updateUserSettings]');
                });
        },
        updateVersion: function (entity, appId, version) {
            R.noti.loading();
            $('#item_' + appId).addClass('updating');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Service/updateVersion',
                data: 'entity=' + entity + '&appId=' + appId + '&version=' + version
            })
                .done(function (json) {
                    if (json.result !== 2) {
                        R.noti.render(json.message);
                        // publishing, deleting, undeleting
                        $('#item_' + appId).remove();
                        var $items = $('#item-count');
                        $items.html(parseInt($items.html()) - 1);
                    } else {
                        R.noti.error(json.message);
                    }
                })
                .fail(function (XMLHttpRequest, textStatus) {
                    R.noti.error(textStatus + ' [updateVersion]');
                });
        }
    };

    R.editor = (function (editor) {
        editor.richText = function () {
            tinymce.init({
                selector: 'textarea.richText',
                theme: 'modern',
                width: '100%',
                height: 300,
                fix_list_elements: true,
                content_css: '/node_modules/rebond-assets-admin/css/rebond-editor.css',
                image_list: '/service/imageList',
                language_url: '/node_modules/rebond-assets-admin/js/' + $('#js-lang').data('locale') + '.js',
                setup: function (ed) {
                    ed.on('change', function (e) {
                        R.formChanged = true;
                    });
                },
                menu: {
                    view: {title: 'View', items: 'preview, fullscreen, code | print'},
                    edit: {title: 'Edit', items: 'undo, redo | cut, copy, paste, pastetext | selectall, searchreplace'},
                    insert: {title: 'Insert', items: 'media, link, image, charmap'},
                    format: {
                        title: 'Format',
                        items: 'bold, italic, underline, strikethrough, superscript, subscript | formats | removeformat'
                    },
                    table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
                },
                toolbar: 'undo redo | bold italic underline | forecolor backcolor | fontsizeselect | bullist numlist outdent indent | image | removeformat',
                plugins: 'autolink, preview, fullscreen, code, print, paste, searchreplace, media, link, image, charmap, table, lists, advlist, textcolor, colorpicker',
            });
        };

        editor.mediaModalSelector = function () {
            var $modal = $('#modal-rebond');
            // change page
            $modal.on('click', '.rb-paging a', function (e) {
                e.preventDefault();
                var hash = $(this).attr('href');
                var field = $('#media-selected').val();
                var search = $('#search-media').val();
                R.service.selectMedia(field, hash, search);
            });

            // filters
            $modal.on('change', '#view', function () {
                var view = $(this).val();
                R.service.updateUserSettings(view, 'mediaView', 'media-modal');
            });

            $modal.on('change', '#count', function () {
                var count = $(this).val();
                R.service.updateUserSettings(count, 'mediaPaging', 'media-modal');
            });

            $modal.on('change', '#order', function () {
                var field = $('#media-selected').val();
                var hash = $(this).val();
                var search = $('#search-media').val();
                R.service.selectMedia(field, hash, search);
            });

            $modal.on('change', '#folderId', function () {
                var field = $('#media-selected').val();
                var hash = $(this).val();
                var search = $('#search-media').val();
                R.service.selectMedia(field, hash, search);
            });

            $modal.on('change', '#type', function () {
                var field = $('#media-selected').val();
                var hash = $(this).val();
                var search = $('#search-media').val();
                R.service.selectMedia(field, hash, search);
            });

            $modal.on('keyup', '#search-media', function () {
                var field = $('#media-selected').val();
                var hash = $('#hash').val();
                var search = $(this).val();
                R.delay(function () {
                    R.service.selectMedia(field, hash, search);
                }, 500);
            });

            $('.input-media').each(function () {
                var $$ = $(this);
                var field = $$.data('field');
                if ($('#media-' + field).val() === '') {
                    $('#media-clear-' + field).hide();
                } else {
                    $('#media-select-' + field).hide();
                }
            });

            // open medium in modal
            $('a.media-select').click(function (e) {
                e.preventDefault();
                var field = $(this).data('field');
                R.service.selectMedia(field, 1);
            });

            // clear selected media
            $('a.media-clear').click(function (e) {
                e.preventDefault();
                var field = $(this).data('field');
                $('#media-' + field).val('');
                $('#' + field).val(0);
                $('#media-clear-' + field).hide();
                $('#media-select-' + field).show();
            });

            // select media
            $modal.on('click', '.media', function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                var name = $(this).data('title');
                var field = $modal.find('#media-selected').val();
                $('#' + field).val(id);
                $('#media-' + field).val(name);
                $('#media-clear-' + field).show();
                $('#media-select-' + field).hide();
                $modal.slideUp();
                $('#rb-overlay').hide();
            });
        };

        return editor;
    }(R.editor || {}));

    R.tools = {
        checkPlugin: function ($module) {
            var selected = $module.find('option:selected');
            var hasContent = selected.data('content');
            if (hasContent === 0) {
                $('#type').val(4).css('display', 'none');
                $('#type-disabled').show();
            } else {
                $('#type').css('display', 'inline');
                $('#type-disabled').hide();
            }
        },
        setContentFilters: function (change) {
            var module = '0';
            if (change) {
                module = $('#module').find('option:selected').val();
            } else {
                var url = R.url.parseUrl();
                if (url.hash !== '') {
                    module = url.hashList[0];
                    $('#module').val(module);
                }
            }
            if (module !== '0') {
                if (change) {
                    location.hash = '#' + module + '/published';
                }
                $('#status-bar').removeClass('hide');
                $('#published').attr('href', '#' + module + '/published');
                $('#pending').attr('href', '#' + module + '/pending');
                $('#deleted').attr('href', '#' + module + '/deleted');
            } else {
                if (change) {
                    location.hash = '#';
                }
                $('#status-bar').addClass('hide');
            }
        },
        mediaLinkEdit: function ($package) {
            if ($package.val() === 0) { // App
                $('#idField').parent().parent().hide();
                $('#titleField').parent().parent().hide();
                $('#url').parent().parent().hide();
                $('#filter').parent().parent().hide();
            } else {
                $('#idField').parent().parent().show();
                $('#titleField').parent().parent().show();
                $('#url').parent().parent().show();
                $('#filter').parent().parent().show();
            }
        },
        search: function () {
            var html = '<input type="text" class="input" id="search-content" placeholder="' + lang.search + '"/><div id="search-result"></div>';
            R.modal.create(html, lang.content_search);
            $('#search-content').focus();
        },
        toFriendlyUrl: function (text) {
            var r = text.toLowerCase();
            r = r.replace(/\s+/ig, '-');
            r = r.replace(/[àáâãäå]/ig, 'a');
            r = r.replace(/æ/ig, 'ae');
            r = r.replace(/ç/ig, 'c');
            r = r.replace(/[èéêë]/ig, 'e');
            r = r.replace(/[ìíîï]/ig, 'i');
            r = r.replace(/ñ/ig, 'n');
            r = r.replace(/[òóôõöø]/ig, 'o');
            r = r.replace(/œ/ig, 'oe');
            r = r.replace(/[ùúûü]/ig, 'u');
            r = r.replace(/[ýÿ]/ig, 'y');
            r = r.replace(/\W+/ig, '-');
            r = r.trim();
            return r;
        }
    };

    R.isDev = function () {
        return $('#user-dev').val() === 1;
    };

})(window.R = window.R || {});
