(function($) { 
    $.fn.folder = function(settings) {
        var options = { expand: {} };
        $.extend(options, settings);

        return this.each(function() {
            var $tree = $(this).addClass('tree');

            $(options.expand, $tree).each(function() {
                $('ul ul', $tree).hide();
                $('ul:first', $tree).show();
                $(this).addClass('open');

                $('li', $tree).removeClass('file folder');
                $('li:not(:has(li))', $tree).addClass('file');
                $('li:has(li)', $tree).addClass('folder');

                var id = R.url.hashList(1);
                if (R.isset(id) && id > 0) {
                    enableFolder($('#f_' + id + ' > a'));
                }

                var $selected = $('.selected', $tree);
                if ($selected.length) {
                    var $lis = $selected.parents('li');
                    $lis.each(function(i){
                        if (i >= 1 && i != ($lis.length - 1)) {
                            $(this).addClass('open');
                            $(this).children('ul').toggle();
                        }
                    });
                }
            });

            // open folder
            $('.folder > a > .icon', $tree).click(function(e) {
                var $li = $(this).parent().parent();
                $li.children('ul').toggle();
                $li.toggleClass('open');
                e.stopPropagation();
                return false;
            });

            $('a', $tree).click(function() {
                enableFolder($(this));
            });

            $('li', $tree).click(function(e) {
                e.stopPropagation();
            });

            function enableFolder(title) {
                $('a', $tree).removeClass('selected');
                title.addClass('selected');
                var url = R.url.parseUrl();
                var paging = R.isset(url.hashList[2]) ? url.hashList[2] : 1;
                var folderId = parseInt(title.data('id'));
                title.attr('href', '#' + folderId + '/' + paging);

                $('#folder-add').attr('href', '/media/folder-edit/?pid=' + folderId);
                $('#upload-media').attr('href', '/media/upload/?id=' + folderId);
                if(folderId != 1){
                    $('#folder-edit').removeClass('disabled').attr('href','/media/folder-edit/?id=' + folderId);
                    $('#folder-delete').removeClass('disabled');
                } else {
                    $('#folder-edit').addClass('disabled').attr('href','#');
                    $('#folder-delete').addClass('disabled');
                }
                $('#folder-del').val(folderId);
            }
        });
    };
})(jQuery);